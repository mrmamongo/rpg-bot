from aiogram import Router
from .add_game import router as add_game_router

router = Router()

router.include_router(add_game_router)
