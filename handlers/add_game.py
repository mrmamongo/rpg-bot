import datetime
import logging

from aiogram import Router, F
from aiogram.types import Message
from aiogram.fsm.context import FSMContext
from aiogram.types.reply_keyboard_remove import ReplyKeyboardRemove
from aiogram.filters.command import Command
import sql_statements
from fsm_states import AddGameStates

from asyncpg import Connection

logger = logging.getLogger(__name__)

router = Router()


@router.message(Command("add_game"))
async def handle_add_game(message: Message, state: FSMContext):
    await state.set_state(AddGameStates.system_short)
    await message.answer("Введите название системы (короткое)")


@router.message(Command("cancel"))
@router.message(F.text.casefold() == "cancel")
async def cancel_handler(message: Message, state: FSMContext) -> None:
    current_state = await state.get_state()
    if current_state is None:
        return

    logging.info("Cancelling state %r", current_state)
    await state.clear()
    await message.answer(
        "Cancelled.",
        reply_markup=ReplyKeyboardRemove(),
    )


@router.message(AddGameStates.system_short)
async def handle_system_short(message: Message, state: FSMContext):
    await state.update_data(system_short=message.text)
    await state.set_state(AddGameStates.system_full)
    await message.answer("Введите название системы (полное)")


@router.message(AddGameStates.system_full)
async def handle_system_full(message: Message, state: FSMContext):
    await state.update_data(system_full=message.text)
    await state.set_state(AddGameStates.campaign)
    await message.answer("Введите название кампании")


@router.message(AddGameStates.campaign)
async def handle_campaign(message: Message, state: FSMContext):
    await state.update_data(campaign=message.text)
    await state.set_state(AddGameStates.scenario)
    await message.answer("Введите название сценария")


@router.message(AddGameStates.scenario)
async def handle_scenario(message: Message, state: FSMContext):
    await state.update_data(scenario=message.text)
    await state.set_state(AddGameStates.slots)
    await message.answer("Введите количество слотов")


@router.message(AddGameStates.slots)
async def handle_slots(message: Message, state: FSMContext):
    try:
        slots = int(message.text)
    except ValueError:
        await message.answer("Введите число")
        return

    await state.update_data(slots=slots)
    await state.set_state(AddGameStates.date)
    await message.answer("Введите дату")


@router.message(AddGameStates.date)
async def handle_date(message: Message, state: FSMContext):
    try:
        date = datetime.date.fromisoformat(message.text)
    except ValueError:
        await message.answer("Пожалуйста, введите корректную дату в формате ISO")
        return

    await state.update_data(date=date)
    await state.set_state(AddGameStates.intro)
    await message.answer("Введите вводную к игре")


@router.message(AddGameStates.intro)
async def handle_intro(message: Message, state: FSMContext, db: Connection):
    # intro = message.text
    data = await state.get_data()
    async with db.transaction():
        await message.answer(await db.execute(
            sql_statements.add_game,
            data.get("system_short"),
            data.get("system_full"),
            data.get("campaign"),
            data.get("scenario"),
            data.get("slots"),
            data.get("date"),
            data.get("intro")
        ))
