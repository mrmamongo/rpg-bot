import os
import json
import shutil

from variables import tg_bot
from variables import tg_chat_id

import utils
from menus import create_main_menu

from handlers_.main_menu import handle_main_menu
from handlers_.show_games import handle_show_games
from handlers_.game_info import handle_game_info

from handlers_.add_game import handle_add_game
from handlers_.notify import handle_notify
from handlers_.delete_game import handle_delete_game
from handlers_.edit_game import handle_edit_game_menu
from handlers_.edit_game import handle_edit_game_text
from handlers_.change_player_name import handle_change_player_name
from handlers_.kick_player_menu import handle_kick_player_menu
from handlers_.kick_player import handle_kick_player
from handlers_.add_player import handle_add_player

not_in_chat_msg = "Привет! Бот написан для небоьшой группы людей, которые состоят в одном чате. Прости, но мы пока принимаем только по связям: знакомых и знакомых знакомых."


@tg_bot.message_handler(commands=['start'])
def handle_start(message):
    try:
        tg_bot.get_chat_member(tg_chat_id, message.from_user.id)
    except:
        tg_bot.send_message(message.chat.id, not_in_chat_msg)
        return

    if message.chat.id != message.from_user.id:
        tg_bot.send_message(message.chat.id, "Привет! Я работаю только в ЛС!")
        return

    tg_bot.send_message(message.chat.id, "Привет! Что ты хочешь сделать?", reply_markup=create_main_menu())


@tg_bot.message_handler(commands=['cancel'])
def handle_cancel(message):
    try:
        tg_bot.get_chat_member(tg_chat_id, message.from_user.id)
    except:
        tg_bot.send_message(message.chat.id, not_in_chat_msg)
        return

    if message.chat.id != message.from_user.id:
        return

    if os.path.exists(f'./sessions/{message.from_user.id}'):
        shutil.rmtree(f'./sessions/{message.from_user.id}')
        tg_bot.send_message(message.chat.id, "Действие было отменено!")
    else:
        tg_bot.send_message(message.chat.id, "Вы и так ничего не делали")


@tg_bot.message_handler(content_types=['text'])
def handle_text(message):
    try:
        tg_bot.get_chat_member(tg_chat_id, message.from_user.id)
    except:
        tg_bot.send_message(message.chat.id, not_in_chat_msg)
        return

    if message.chat.id != message.from_user.id:
        tg_bot.send_message(message.chat.id, "Привет! Я работаю только в ЛС!")
        return

    if os.path.exists(f'./sessions/{message.from_user.id}'):
        f = open(f'./sessions/{message.from_user.id}/session.json', encoding='utf-8')
        session = json.load(f)
        f.close()
        if session["action"] == 'add-game':
            handle_add_game(message.text, message.chat.id, message.from_user.id)
        elif session["action"] == "notify":
            handle_notify(message.text, message.chat.id, message.from_user.id)
        elif session["action"] == "delete":
            handle_delete_game(message.text, message.chat.id, message.from_user.id)
        elif session["action"] == "edit-game":
            handle_edit_game_text(message.text, message.chat.id, message.from_user.id)
        elif session["action"] == "change-player-name":
            handle_change_player_name(message.text, message.chat.id, message.from_user.id)
        elif session["action"] == "kick-player":
            handle_kick_player(message.text, message.chat.id, message.from_user.id)
        elif session["action"] == "add-player":
            handle_add_player(message.text, message.chat.id, message.from_user.id)


@tg_bot.callback_query_handler(func=lambda call: True)
def main_callback(call):
    chat_id = call.message.chat.id
    message_id = call.message.message_id
    user = call.from_user

    try:
        tg_bot.get_chat_member(tg_chat_id, user.id)
    except:
        tg_bot.send_message(chat_id, not_in_chat_msg)
        return

    if chat_id != user.id:
        tg_bot.send_message(chat_id, "Привет! Я работаю только в ЛС!")
        return

    if call.data.startswith('main_'):
        command = call.data.replace('main_', '')
        reply = handle_main_menu(command, chat_id, message_id, user)
    elif call.data.startswith('show-games_'):
        command = call.data.replace('show-games_', '')
        reply = handle_show_games(command, chat_id, message_id, user)
    elif call.data.startswith('game-info_'):
        command = call.data.replace('game-info_', '')
        reply = handle_game_info(command, chat_id, message_id, user)
    elif call.data.startswith('edit-game_'):
        command = call.data.replace('edit-game_', '')
        reply = handle_edit_game_menu(command, chat_id, message_id, user)
    elif call.data.startswith('kick-player_'):
        command = call.data.replace('kick-player_', '')
        reply = handle_kick_player_menu(command, chat_id, message_id, user)
    else:
        reply = "Внутренняя ошибка!"
    if reply:
        tg_bot.answer_callback_query(callback_query_id=call.id, text=reply, show_alert=True)
    else:
        tg_bot.answer_callback_query(callback_query_id=call.id)


while True:
    try:
        tg_bot.infinity_polling()
    except Exception as e:
        utils.save_exception('main_polling', e)
        continue
