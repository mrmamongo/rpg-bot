import json
import os
import shutil

import utils

from variables import tg_bot

from menus import get_game_info_header
from menus import create_game_info_menu


def handle_kick_player_menu(command, chat_id, message_id, user):
    if command.startswith('go-back_'):
        game_id = command.replace('go-back_', '')

        tg_bot.edit_message_text(get_game_info_header(game_id), chat_id=chat_id, message_id=message_id, parse_mode='HTML')
        tg_bot.edit_message_reply_markup(chat_id=chat_id, message_id=message_id,
                                         reply_markup=create_game_info_menu(game_id, user))
        return str()
    elif command.startswith('kick_'):
        game_id = command.replace('kick_', '').split('_')[0]
        player_num = command.replace('kick_', '').split('_')[1]

        if os.path.exists(f'./sessions/{user.id}'):
            shutil.rmtree(f'./sessions/{user.id}')
        os.makedirs(f'./sessions/{user.id}')

        session = dict()
        session["action"] = "kick-player"
        session["game_id"] = game_id
        session["player_num"] = player_num

        utils.save_to_file(f'./sessions/{user.id}/session.json', json.dumps(session, indent=4))

        tg_bot.edit_message_text(
            'Хорошо! Напиши "Подтверждаю", если ты уверен(а). Если напишешь что-то другое - действие будет отменено.',
            chat_id=chat_id, message_id=message_id)

        return str()
