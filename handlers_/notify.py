import json
import shutil

from variables import tg_bot

import utils


def handle_notify(text, chat_id, user_id):
    f = open(f'./sessions/{user_id}/session.json', encoding='utf-8')
    session = json.load(f)
    f.close()

    if session["stage"] == "compose":
        session["message"] = text
        tg_bot.send_message(chat_id, 'Хорошо сказано! Проверь ещё раз и отправьте сообщение "Подтверждаю", чтобы отправить сообщение игрокам. Если ты отправишь что-то другое - то отправка будет отменена.')
        session["stage"] = "confirm"
        utils.save_to_file(f'./sessions/{user_id}/session.json', json.dumps(session, indent=4))
    elif session["stage"] == "confirm":
        if text != 'Подтверждаю':
            tg_bot.send_message(chat_id, 'Отправка сообщение отменена!')
            return
        game_id = session["game_id"]
        f = open(f'./games/{game_id}.json', encoding='utf-8')
        game_info = json.load(f)
        f.close()

        game_header = f'[{game_info["system_short"]}]{game_info["campaign"]} - {game_info["scenario"]}'

        message = session["message"]
        for player in game_info["players"]:
            if player != -1:
                tg_bot.send_message(player, f"Привет! Ведущий просил передать информацию об игре, на которую ты записан(а).\n{game_header}")
                tg_bot.send_message(player, message)

        tg_bot.send_message(chat_id, 'Сообщение отправлено!')

        shutil.rmtree(f'./sessions/{user_id}')
