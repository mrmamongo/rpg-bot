import os
import json
import shutil

from variables import tg_bot
from variables import tg_chat_id

import utils

from menus import get_show_games_header
from menus import create_show_games_menu

from menus import get_edit_game_header
from menus import create_edit_game_menu

from menus import get_kick_player_header
from menus import create_kick_player_menu


def handle_game_info(command, chat_id, message_id, user):
    if command == 'go-back':
        tg_bot.edit_message_text(get_show_games_header(), chat_id=chat_id, message_id=message_id)
        tg_bot.edit_message_reply_markup(chat_id=chat_id, message_id=message_id,
                                         reply_markup=create_show_games_menu())
        return str()
    elif command.startswith('notify_'):
        game_id = command.replace('notify_', '')

        tg_bot.edit_message_text("Хорошо! Напиши, что ты хочешь, чтобы я им отправил", chat_id=chat_id,
                                 message_id=message_id)

        if os.path.exists(f'./sessions/{user.id}'):
            shutil.rmtree(f'./sessions/{user.id}')
        os.makedirs(f'./sessions/{user.id}')
        session = dict()

        session["game_id"] = game_id
        session["action"] = "notify"
        session["stage"] = "compose"

        utils.save_to_file(f'./sessions/{user.id}/session.json', json.dumps(session, indent=4))

        return str()
    elif command.startswith('change-player-name_'):
        game_id = command.replace('change-player-name_', '')

        tg_bot.edit_message_text("Хорошо! Напиши, как ты хочешь, чтобы я тебя подписывал", chat_id=chat_id,
                                 message_id=message_id)

        if os.path.exists(f'./sessions/{user.id}'):
            shutil.rmtree(f'./sessions/{user.id}')
        os.makedirs(f'./sessions/{user.id}')

        session = dict()
        session["game_id"] = game_id
        session["action"] = "change-player-name"
        session["stage"] = "compose"

        utils.save_to_file(f'./sessions/{user.id}/session.json', json.dumps(session, indent=4))

        return str()
    elif command.startswith('open-reg_'):
        game_id = command.replace('open-reg_', '')

        f = open(f'./games/{game_id}.json', encoding='utf-8')
        game_json = json.load(f)
        f.close()

        game_json["registration_open"] = True
        utils.save_to_file(f'./games/{game_id}.json', json.dumps(game_json, indent=4))

        tg_bot.edit_message_text(get_show_games_header(), chat_id=chat_id, message_id=message_id)
        tg_bot.edit_message_reply_markup(chat_id=chat_id, message_id=message_id,
                                         reply_markup=create_show_games_menu())

        return "Регистрация открыта!"
    elif command.startswith('close-reg_'):
        game_id = command.replace('close-reg_', '')

        f = open(f'./games/{game_id}.json', encoding='utf-8')
        game_json = json.load(f)
        f.close()

        game_json["registration_open"] = False
        utils.save_to_file(f'./games/{game_id}.json', json.dumps(game_json, indent=4))

        tg_bot.edit_message_text(get_show_games_header(), chat_id=chat_id, message_id=message_id)
        tg_bot.edit_message_reply_markup(chat_id=chat_id, message_id=message_id,
                                         reply_markup=create_show_games_menu())

        return "Регистрация закрыта!"
    elif command.startswith('bump-session_'):
        game_id = command.replace('bump-session_', '')

        f = open(f'./games/{game_id}.json', encoding='utf-8')
        game_json = json.load(f)
        f.close()

        game_json["session"] += 1
        utils.save_to_file(f'./games/{game_id}.json', json.dumps(game_json, indent=4))

        tg_bot.edit_message_text(get_show_games_header(), chat_id=chat_id, message_id=message_id)
        tg_bot.edit_message_reply_markup(chat_id=chat_id, message_id=message_id,
                                         reply_markup=create_show_games_menu())

        return "Номер сессии увеличен!"
    elif command.startswith('edit_'):
        game_id = command.replace('edit_', '')

        tg_bot.edit_message_text(get_edit_game_header(), chat_id=chat_id, message_id=message_id)
        tg_bot.edit_message_reply_markup(chat_id=chat_id, message_id=message_id,
                                         reply_markup=create_edit_game_menu(game_id))

        return str()
    elif command.startswith('delete_'):
        game_id = command.replace('delete_', '')

        tg_bot.edit_message_text('Хорошо! Напиши "Подтверждаю", если ты уверен(а). Если напишешь что-то другое - удаление будет отменено.', chat_id=chat_id,
                                 message_id=message_id)

        if os.path.exists(f'./sessions/{user.id}'):
            shutil.rmtree(f'./sessions/{user.id}')
        os.makedirs(f'./sessions/{user.id}')
        session = dict()

        session["game_id"] = game_id
        session["action"] = "delete"
        session["stage"] = "confirm"

        utils.save_to_file(f'./sessions/{user.id}/session.json', json.dumps(session, indent=4))

        return str()
    elif command.startswith('register_'):
        game_id = command.replace('register_', '')
        f = open(f'./games/{game_id}.json', encoding='utf-8')
        game_json = json.load(f)
        f.close()

        game_json["players"].append(user.id)
        game_json["player_names"].append(f'{user.full_name}')
        game_json["free_slots"] -= 1

        game_header = f'[{game_json["system_short"]}]{game_json["campaign"]} - {game_json["scenario"]}'
        game_header = game_header.replace('<', '&lt;')
        game_header = game_header.replace('>', '&gt;')
        game_header = game_header.replace('&', '&amp;')

        gm_msg = f"Привет, обновление по твоей игре\n{game_header}\n"

        player_user = tg_bot.get_chat_member(tg_chat_id, user.id).user
        gm_msg += f'На твою игру записался <a href = "tg://user?id={player_user.id}">{player_user.full_name}</a> (@{player_user.username})'

        if game_json["free_slots"] == 0:
            gm_msg += '\nТеперь на неё записано максимальное число игроков'

        tg_bot.send_message(game_json["gm"], gm_msg, parse_mode="HTML")

        utils.save_to_file(f'./games/{game_id}.json', json.dumps(game_json, indent=4))

        tg_bot.edit_message_text(get_show_games_header(), chat_id=chat_id, message_id=message_id)
        tg_bot.edit_message_reply_markup(chat_id=chat_id, message_id=message_id,
                                         reply_markup=create_show_games_menu())

        return 'Вы успешно записались на игру!'
    elif command.startswith('unregister_'):
        game_id = command.replace('unregister_', '')
        f = open(f'./games/{game_id}.json', encoding='utf-8')
        game_json = json.load(f)
        f.close()

        for i in range(len(game_json["players"])):
            if game_json["players"][i] == user.id:
                del game_json["players"][i]
                del game_json["player_names"][i]
                break
        game_json["free_slots"] += 1
        utils.save_to_file(f'./games/{game_id}.json', json.dumps(game_json, indent=4))

        tg_bot.edit_message_text(get_show_games_header(), chat_id=chat_id, message_id=message_id)
        tg_bot.edit_message_reply_markup(chat_id=chat_id, message_id=message_id,
                                         reply_markup=create_show_games_menu())

        return 'Вы успешно отменили запись на игру!'
    elif command.startswith('add-player_'):
        game_id = command.replace('add-player_', '')

        if os.path.exists(f'./sessions/{user.id}'):
            shutil.rmtree(f'./sessions/{user.id}')
        os.makedirs(f'./sessions/{user.id}')

        session = dict()
        session["action"] = "add-player"
        session["game_id"] = game_id
        session["stage"] = "compose"

        utils.save_to_file(f'./sessions/{user.id}/session.json', json.dumps(session, indent=4))

        tg_bot.edit_message_text('Хорошо, напиши, как мне его записать', chat_id=chat_id, message_id=message_id)

        return str()
    elif command.startswith('kick-player_'):
        game_id = command.replace('kick-player_', '')

        tg_bot.edit_message_text(get_kick_player_header(), chat_id=chat_id, message_id=message_id)
        tg_bot.edit_message_reply_markup(chat_id=chat_id, message_id=message_id,
                                         reply_markup=create_kick_player_menu(game_id))

        return str()
