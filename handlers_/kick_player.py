import json
import shutil

import utils

from variables import tg_bot


def handle_kick_player(text, chat_id, user_id):
    if text == "Подтверждаю":
        f = open(f'./sessions/{user_id}/session.json', encoding='utf-8')
        session = json.load(f)
        f.close()

        game_id = session["game_id"]
        player_num = int(session["player_num"])

        f = open(f'./games/{game_id}.json', encoding='utf-8')
        game_json = json.load(f)
        f.close()

        if game_json["players"][player_num] != -1:
            game_header = f'[{game_json["system_short"]}]{game_json["campaign"]} - {game_json["scenario"]}'
            tg_bot.send_message(game_json["players"][player_num], f'Привет, сообщаю, что тебя выгнали с игры\n{game_header}')

        del game_json["players"][player_num]
        del game_json["player_names"][player_num]

        game_json["free_slots"] += 1
        utils.save_to_file(f'./games/{game_id}.json', json.dumps(game_json, indent=4))

        tg_bot.send_message(chat_id, 'Игрок успешно выгнан!')
    else:
        tg_bot.send_message(chat_id, 'Вы отправили сообщение, отличное от "Подтверждаю" - удаление игры отменено')

    shutil.rmtree(f'./sessions/{user_id}')
