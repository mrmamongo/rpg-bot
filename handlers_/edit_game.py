import os
import json
import shutil

import utils

from variables import tg_bot

from menus import get_game_info_header, create_game_info_menu


def handle_edit_game_menu(command, chat_id, message_id, user):
    if os.path.exists(f'./sessions/{user.id}'):
        shutil.rmtree(f'./sessions/{user.id}')
    os.makedirs(f'./sessions/{user.id}')
    session = dict()

    session["action"] = 'edit-game'
    session["stage"] = 'compose'

    if command.startswith('system-short_'):
        game_id = command.replace('system-short_', '')
        session["game_id"] = game_id
        session["parameter"] = 'system_short'
        tg_bot.edit_message_text('Напиши новое краткое название системы', chat_id, message_id)
    elif command.startswith('system-full_'):
        game_id = command.replace('system-full_', '')
        session["game_id"] = game_id
        session["parameter"] = 'system_full'
        tg_bot.edit_message_text('Напиши новое полное название системы', chat_id, message_id)
    elif command.startswith('campaign_'):
        game_id = command.replace('campaign_', '')
        session["game_id"] = game_id
        session["parameter"] = 'campaign'
        tg_bot.edit_message_text('Напиши новое название кампании', chat_id, message_id)
    elif command.startswith('scenario_'):
        game_id = command.replace('scenario_', '')
        session["game_id"] = game_id
        session["parameter"] = 'scenario'
        tg_bot.edit_message_text('Напиши новое название сценария', chat_id, message_id)
    elif command.startswith('session_'):
        game_id = command.replace('session_', '')
        session["game_id"] = game_id
        session["parameter"] = 'session'
        tg_bot.edit_message_text('Напиши новый номер сессии', chat_id, message_id)
    elif command.startswith('slots_'):
        game_id = command.replace('slots_', '')
        session["game_id"] = game_id
        session["parameter"] = 'slots'
        tg_bot.edit_message_text('Напиши новое максимальное количество игроков', chat_id, message_id)
    elif command.startswith('date_'):
        game_id = command.replace('date_', '')
        session["game_id"] = game_id
        session["parameter"] = 'date'
        tg_bot.edit_message_text('Напиши новую дату', chat_id, message_id)
    elif command.startswith('intro_'):
        game_id = command.replace('intro_', '')
        session["game_id"] = game_id
        session["parameter"] = 'intro'
        tg_bot.edit_message_text('Напиши новую вводную', chat_id, message_id)
    elif command.startswith('gm-name_'):
        game_id = command.replace('gm-name_', '')
        session["game_id"] = game_id
        session["parameter"] = 'gm_name'
        tg_bot.edit_message_text('Напиши, как мне тебе называть', chat_id, message_id)
    elif command.startswith('go-back_'):
        game_id = command.replace('go-back_', '')

        tg_bot.edit_message_text(get_game_info_header(game_id), chat_id=chat_id, message_id=message_id, parse_mode='HTML')
        tg_bot.edit_message_reply_markup(chat_id=chat_id, message_id=message_id,
                                         reply_markup=create_game_info_menu(game_id, user))

        shutil.rmtree(f'./sessions/{user.id}')
        return str()

    utils.save_to_file(f'./sessions/{user.id}/session.json', json.dumps(session, indent=4))

    return str()


def handle_edit_game_text(text, chat_id, user_id):
    f = open(f'./sessions/{user_id}/session.json', encoding='utf-8')
    session = json.load(f)
    f.close()

    game_id = session["game_id"]
    f = open(f'./games/{game_id}.json', encoding='utf-8')
    game_json = json.load(f)
    f.close()

    if session["stage"] == 'compose':
        if session["parameter"] == 'slots':
            try:
                value = int(text)
                if value < len(game_json["players"]):
                    tg_bot.send_message(chat_id, 'Упс! На игру уже записалось больше игроков, чем новый лимит. Если всё равно хочешь уменьшить число игроков - сначала попроси их отменить запись или свяжись с администратором. Чтобы отменить изменение введи команду /cancel')
                    return
            except:
                tg_bot.send_message(chat_id, 'Упс! Кажется, ты отправил(а) что-то кроме числа. Количество мест на игре должно быть числом без дополнительных символов')
                return
        elif session["parameter"] == 'session':
            try:
                value = int(text)
            except:
                tg_bot.send_message(chat_id, 'Упс! Кажется, ты отправил(а) что-то кроме числа. Номер сессии должен быть числом без дополнительных символов')
                return
        else:
            value = text
        session["value"] = value
        session["stage"] = 'confirm'

        tg_bot.send_message(chat_id, 'Отлично! Проверь ещё раз и напиши "Подтверждаю", чтобы применить изменения. Если отправишь что-то другое - изменения будут отменены')

        utils.save_to_file(f'./sessions/{user_id}/session.json', json.dumps(session, indent=4))
    elif session["stage"] == "confirm":
        if text != 'Подтверждаю':
            tg_bot.send_message(chat_id, 'Вы отправили сообщение, отличное от "Подтверждаю" - редактирование игры отменено')
        else:
            game_json[session["parameter"]] = session["value"]
            if session["parameter"] == 'slots':
                game_json["free_slots"] = game_json["slots"] - len(game_json["players"])
            utils.save_to_file(f'./games/{game_id}.json', json.dumps(game_json, indent=4))
            tg_bot.send_message(chat_id, 'Игра успешно изменена!')

        shutil.rmtree(f'./sessions/{user_id}')
