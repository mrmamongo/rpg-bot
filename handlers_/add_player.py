import json
import shutil

import utils

from variables import tg_bot


def handle_add_player(text, chat_id, user_id):
    f = open(f'./sessions/{user_id}/session.json', encoding='utf-8')
    session = json.load(f)
    f.close()

    if session["stage"] == 'compose':
        session["name"] = text
        session["stage"] = 'confirm'

        utils.save_to_file(f'./sessions/{user_id}/session.json', json.dumps(session, indent=4))

        tg_bot.send_message(chat_id, 'Отлично! Проверь ещё раз и напиши "Подтверждаю", чтобы подтвердить. Если отправишь что-то другое - добавление будет отменено')
    elif session["stage"] == 'confirm':
        if text != 'Подтверждаю':
            tg_bot.send_message(chat_id, 'Вы отправили сообщение, отличное от "Подтверждаю" - редактирование игры отменено')
        else:
            game_id = session["game_id"]
            f = open(f'./games/{game_id}.json', encoding='utf-8')
            game_json = json.load(f)
            f.close()

            game_json["players"].append(-1)
            game_json["player_names"].append(session["name"])
            game_json["free_slots"] -= 1

            utils.save_to_file(f'./games/{game_id}.json', json.dumps(game_json, indent=4))

        tg_bot.send_message(chat_id, 'Игрок добавлен!')
        shutil.rmtree(f'./sessions/{user_id}')
