import os
import json
import shutil

from variables import tg_bot

import utils

from menus import get_show_games_header
from menus import create_show_games_menu


def handle_main_menu(command, chat_id, message_id, user):
    if command == 'show-games':
        tg_bot.edit_message_text(get_show_games_header(), chat_id=chat_id, message_id=message_id)
        tg_bot.edit_message_reply_markup(chat_id=chat_id, message_id=message_id,
                                         reply_markup=create_show_games_menu())
        return str()
    elif command == 'add-game':
        tg_bot.edit_message_text('Хорошо! Тебе нужно будет рассказать информацию об игре. Напиши краткое название игровой системы. Например, "D&D 5e"',
                                 chat_id=chat_id,
                                 message_id=message_id)

        if os.path.exists(f'./sessions/{user.id}'):
            shutil.rmtree(f'./sessions/{user.id}')
        os.makedirs(f'./sessions/{user.id}')

        session = dict()
        session["action"] = "add-game"
        session["stage"] = "system_short"

        game_info = dict()
        game_info["gm"] = user.id
        game_info["gm_name"] = f'{user.full_name}'
        game_info["session"] = 1
        game_info["players"] = []
        game_info["player_names"] = []
        game_info["registration_open"] = True

        utils.save_to_file(f'./sessions/{user.id}/session.json', json.dumps(session, indent=4))
        utils.save_to_file(f'./sessions/{user.id}/game_info.json', json.dumps(game_info, indent=4))
    elif command == 'exit':
        tg_bot.edit_message_text('Пока 👋! Если ещё понадоблюсь, введи /start', chat_id=chat_id, message_id=message_id)
        return str()
