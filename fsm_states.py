from aiogram.fsm.state import State, StatesGroup


class AddGameStates(StatesGroup):
    system_short = State()
    system_full = State()
    campaign = State()
    scenario = State()
    slots = State()
    date = State()
    intro = State()
