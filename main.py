import asyncio
import logging

from aiogram import Bot, Dispatcher
from aiogram.fsm.storage.redis import RedisStorage
from aiogram.fsm.storage.memory import MemoryStorage
from redis import Redis
import utils
from config import settings
from handlers import router
from middlewares import DatabaseMiddleware
from asyncpg import create_pool

logger = logging.getLogger(__name__)


async def register_middleware(dp: Dispatcher) -> None:
    pool = await create_pool(settings.database.dsn)
    dp.update.middleware(DatabaseMiddleware(pool))


async def register_handlers(dp: Dispatcher) -> None:
    dp.include_router(router)


async def main() -> None:
    bot = Bot(token=settings.telegram.token)
    storage = MemoryStorage()
    dp = Dispatcher(storage=storage)

    await register_middleware(dp)
    await register_handlers(dp)

    try:
        await dp.start_polling(bot)
    finally:
        await dp.storage.close()


if __name__ == '__main__':
    try:
        asyncio.run(main())
    except KeyboardInterrupt:
        logger.info('Bot stopped by user')
    except Exception as e:
        utils.save_exception('main_polling', e)
        logger.exception(e)
