# RPG Bot
Бот для менеджмента игр в НРИ
## Внимание! Проект является хобби и код написан не самым лучшим образом

# Скриншоты
![Main Menu](screenshots/main_menu.jpg)
![Games List Menu](screenshots/games_list_menu.jpg)
![Games Info Menu (as player)](screenshots/game_info_menu_player.jpg)
![Games Info Menu (as DM)](screenshots/game_info_menu_dm.jpg)
![Game Edit Menu](screenshots/edit_menu.jpg)


# Установка
## Зависимости
- pyTelegramBotApi
## Настройка
Необходимо создать файл variables.py следующего формата
```Python
import telebot

tg_bot_token = # yor token for Telegram goes here
tg_bot = telebot.TeleBot(tg_bot_token)
tg_chat_id = # your ascociated rpg chat goest here
```

ID чата нужен для того, чтобы люди со стороны не могли записываться на игры.

## Запуск
Бот не принимает параметров на вход, для запуска достаточно команды
`python3 bot.py`
