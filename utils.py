from datetime import datetime


def get_from_file(filename: str):
    with open(filename, 'r', encoding='utf-8') as f:
        return f.read()


def save_to_file(filename: str, val):
    with open(filename, 'w', encoding='utf-8') as f:
        f.write(str(val))


def save_exception(subfolder, e: Exception):
    filename = './exceptions/' + subfolder + datetime.now().isoformat()
    save_to_file(filename, str(e))
