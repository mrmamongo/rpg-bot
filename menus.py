import os
import json

from aiogram import types

from config import settings


def get_main_menu_header():
    return "Что ты хочешь сделать?"


def create_main_menu():
    keyboard = [
        [types.InlineKeyboardButton(text='Открыть список игр', callback_data='main_show-games')],
        [types.InlineKeyboardButton(text='Создать игру', callback_data='main_add-game')],
        [types.InlineKeyboardButton(text='Выйти', callback_data='main_exit')],
    ]

    return types.InlineKeyboardMarkup(keyboard=keyboard, resize_keyboard=True)


def get_show_games_header():
    return "Нажми на игру, чтобы узнать подробнее и показать список доступных действий"


def create_show_games_menu():
    keyboard = []

    game_ids = []
    for filename in os.listdir("./games"):
        if filename.endswith(".json"):
            game_id = int(filename.replace('.json', ''))
            game_ids.append(game_id)

    for game_id in sorted(game_ids):
        f = open(f'./games/{game_id}.json', encoding="utf8")
        game_json = json.load(f)

        if game_json["registration_open"]:
            status = 'O'
        else:
            status = 'C'

        if game_json["campaign"] == 'Ваншот':
            name = f'{game_json["campaign"]} - {game_json["scenario"]}'
        else:
            name = f'{game_json["campaign"]} (сессия {game_json["session"]})'

        game_info = f'[{game_json["system_short"]}][{status}] {name} ({game_json["free_slots"]}/{game_json["slots"]})'
        keyboard.append([types.InlineKeyboardButton(text=game_info, callback_data=f'show-games_{game_id}')])

    keyboard.append([types.InlineKeyboardButton(text='- Назад -', callback_data=f'show-games_go-back')])

    return types.InlineKeyboardMarkup(keyboard)


def get_game_info_header(game_id):
    f = open(f'./games/{game_id}.json', encoding="utf8")
    game_json = json.load(f)

    if game_json["registration_open"]:
        status = 'OPEN'
    else:
        status = 'CLOSED'

    if game_json["campaign"] == 'Ваншот':
        name = f'{game_json["campaign"]} - {game_json["scenario"]}'
    else:
        name = f'{game_json["campaign"]} (сессия {game_json["session"]})'
    
    game_info = f'[{game_json["system_short"]}][{status}] {name}\n - Сценарий: {game_json["scenario"]}\n - Система: {game_json["system_full"]}\n - Ведущий: <a href="tg://user?id={game_json["gm"]}">{game_json["gm_name"]}</a>\n'
    game_info += f' - Дата: {game_json["date"]}\n - Свободные слоты: {game_json["free_slots"]}/{game_json["slots"]}\n'
    game_info += f' - Преамбула:\n{game_json["intro"]}\n - Список записавшихся:\n'

    for i in range(len(game_json["players"])):
        if game_json["players"][i] != -1:
            player_user = tg_bot.get_chat_member(tg_chat_id, game_json["players"][i]).user
            game_info += f'<a href="tg://user?id={game_json["players"][i]}">{game_json["player_names"][i]}</a> (@{player_user.username})\n'
        else:
            game_info += f'{game_json["player_names"][i]} (+1)'

    if not game_json['players']:
        game_info += 'Пока никто не записался'

    return game_info


def create_game_info_menu(game_id, user):
    keyboard = []

    f = open(f'./games/{game_id}.json', encoding="utf8")
    game_json = json.load(f)

    if user.id == game_json["gm"]:
        keyboard.append([types.InlineKeyboardButton(text='Уведомление игрокам', callback_data=f'game-info_notify_{game_id}')])
        if game_json["registration_open"]:
            keyboard.append([types.InlineKeyboardButton(text='Закрыть регистрацию', callback_data=f'game-info_close-reg_{game_id}')])
        else:
            keyboard.append([types.InlineKeyboardButton(text='Открыть регистрацию', callback_data=f'game-info_open-reg_{game_id}')])
        keyboard.append([types.InlineKeyboardButton(text='Увеличить номер сессии', callback_data=f'game-info_bump-session_{game_id}')])
        if game_json['free_slots'] > 0:
            keyboard.append([types.InlineKeyboardButton(text='Добавить игрока', callback_data=f'game-info_add-player_{game_id}')])
        if game_json['free_slots'] < game_json['slots']:
            keyboard.append([types.InlineKeyboardButton(text='Выгнать игрока', callback_data=f'game-info_kick-player_{game_id}')])
        keyboard.append([types.InlineKeyboardButton(text='Редактировать', callback_data=f'game-info_edit_{game_id}')])
        keyboard.append([types.InlineKeyboardButton(text='Удалить', callback_data=f'game-info_delete_{game_id}')])
    else:
        if game_json["registration_open"]:
            if user.id in game_json["players"]:
                keyboard.append([types.InlineKeyboardButton(text='Поменять имя', callback_data=f'game-info_change-player-name_{game_id}')])
                keyboard.append([types.InlineKeyboardButton(text='Отменить регистрацию', callback_data=f'game-info_unregister_{game_id}')])
            else:
                if game_json["free_slots"] > 0:
                    keyboard.append([types.InlineKeyboardButton(text='Зарегистрироваться', callback_data=f'game-info_register_{game_id}')])

    keyboard.append([types.InlineKeyboardButton(text='- Назад -', callback_data=f'game-info_go-back')])

    return types.InlineKeyboardMarkup(keyboard)


def get_kick_player_header():
    return 'Выбери, какого игрока хочешь выгнать'


def create_kick_player_menu(game_id):
    keyboard = []

    f = open(f'./games/{game_id}.json', encoding="utf8")
    game_json = json.load(f)
    f.close()

    for i in range(len(game_json["players"])):
        keyboard.append([types.InlineKeyboardButton(text=f'{game_json["player_names"][i]}', callback_data=f'kick-player_kick_{game_id}_{i}')])
    keyboard.append([types.InlineKeyboardButton(text='- Назад -', callback_data=f'kick-player_go-back_{game_id}')])

    return types.InlineKeyboardMarkup(keyboard)


def get_edit_game_header():
    return "Выбери, что хочешь поменять"


def create_edit_game_menu(game_id):
    keyboard = []

    keyboard.append([types.InlineKeyboardButton(text='Краткое название системы', callback_data=f'edit-game_system-short_{game_id}')])
    keyboard.append([types.InlineKeyboardButton(text='Полное название системы', callback_data=f'edit-game_system-full_{game_id}')])
    keyboard.append([types.InlineKeyboardButton(text='Название кампании', callback_data=f'edit-game_campaign_{game_id}')])
    keyboard.append([types.InlineKeyboardButton(text='Название сценария', callback_data=f'edit-game_scenario_{game_id}')])
    keyboard.append([types.InlineKeyboardButton(text='Номер сессии', callback_data=f'edit-game_session_{game_id}')])
    keyboard.append([types.InlineKeyboardButton(text='Количество мест', callback_data=f'edit-game_slots_{game_id}')])
    keyboard.append([types.InlineKeyboardButton(text='Дата игры', callback_data=f'edit-game_date_{game_id}')])
    keyboard.append([types.InlineKeyboardButton(text='Вводная', callback_data=f'edit-game_intro_{game_id}')])
    keyboard.append([types.InlineKeyboardButton(text='Имя ведущего', callback_data=f'edit-game_gm-name_{game_id}')])
    keyboard.append([types.InlineKeyboardButton(text='- Назад -', callback_data=f'edit-game_go-back_{game_id}')])

    return types.InlineKeyboardMarkup(keyboard)
